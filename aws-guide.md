# AWS

## Steps to Create a Windows BasedVirtual Machine
* Log in to you AWS Console
* Search for Ec2 in the search bar and click on the same
* Click on Launch Instance -> Launch Instance
* Now the first thing you need to select over here is your AMI(Amazon Machine Image)
* Let's select Microsoft Windows Server 2019 Base
* Now it's time to select the Instance Type, just select t2.micro
* Click on Review and Launch
* Click on Launch Button
* On the prompt, on dropdown, just select Create a new Key Pair
* Choose the type as RSA
* On the below text box, define a name for your key pair
* Click on Download Key Pair
* Launch Instances
* Click on the view instances button on the bottom right corner, to get redirected to the Instances List page.

## Getting the password of windows machine
* Select your instance 
* Click on Actions -> Security -> Get Windows Password
* There, upload your pem file you downloaded while creating the server
* Click on Decrypt Password and there you go, you have the credentails over there


## Steps to Create a Linux Based Virtual Machine
* Go to EC2 Dashboard
* Click on Launch Instances
* Now the first thing you need to select over here is your AMI(Amazon Machine Image)
* Let's select Ubuntu Server 18.04 LTS (HVM)
* Now it's time to select the Instance Type, just select t2.micro
* Click on Next Configure Instance Details
* Click on Next Add Storage
* Click on Next Add tags
* Click on Next Configure Security Group
* Click on Review and Launch
* Click on Launch Button
* On the prompt, on dropdown, just select Choose and existing key pair
* On the dropdown below, choose the key pair, which we created in the previous lab
* Click on checkbox saying `I acknowledge .........`
* launch Instances
* Click on the view instances button on the bottom right corner, to get redirected to the Instances List page.



## Log in to Linux VM using Putty
* Converting pem to ppk
    * To convert the key in putty compatible format, open PuttyGen
    * Click on Load
    * Change the filter to All Files in bottom right corner.
    * Browse the pem file which you downloaded while creating the VM
    * Click ok
    * Save private key
    * Click yes
    * Now save the file in any location with any name
    * That's it, now you have a private key in Putty compatible format(ppk)

* In order to login
    * Just open putty
    * Paste your IP in hostname section.
    * In the left pane, expand SSH, click on Auth.
    * In the right side, browse ppk while which you got from puttygen
    * Click open
    * Just provide your username when prompted, (ubuntu)
    * That's it, you have successfully logged into the server

## Installing Webserver in a Ubuntu Based VM
* Switch to Root user

    ```sudo su```
* Update the repo list
        
    ```apt-get update```
* Install Apache2

    ```apt-get install apache2 -y```
* Check Service status
    
    ```service apache2 status```
* Browse it curl
        
    ```curl localhost```

## Allowing traffic on Port in AWS
* Go to your Instance Page
* Click on Security Tab
* Click on Security Group Name
* Click on Edit Inbound Rules
* Add Rule
* Under port - 80
* Select soruce as anywhere
* Save Rules