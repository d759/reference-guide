Most Commonly Used AWS Services

- Compute and Networking Service
	- EC2 (Elastic Compute Cloud)	
	- Lambda (Serverless Computing)
	- Auto Scaling
	- Load Balancers
	- Elastic Beanstalk
	- VPC (Virtual Private Cloud)
	- Route 53

- Storage Services
	- EBS (Elastic Block Storage)
	- EFS (Elastic File Storage)
	- S3 (Simple Storage Service)
	- CloudFront	

- Database Services
	- RDS (Relational Database Service)
	- DyanmoDB
	- Neptune
	- Redshift
	- ElasticCache

- Management Services
	- Cloudwatch
	- CloudTrail
	- CloudFormation
	- IAM

- Other Services
	- SQS (Simple Queue Service)
	- SNS (Simple Notification Service)
	- API Gateway
	- ECS (Elastic Container Service)
	- EKS (Elastic Kubernetes Service)

- DevOps Service
	- AWS Code Commit
	- AWS CodeArtifcat
	- AWS Code Build
	- AWS Code Deploy





